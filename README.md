![NFN](jBD-logo.png)
##NFN是什么?
基于NanoPi neo硬件构建的Node.js环境，以便于业务逻辑开发

##基础环境

```Bash
fs_resize #扩展usb卡，只要一次就可以
```

* 设备名称
```Bash
vi /etc/hostname
直接修改
```

* 网络设置
```Bash
vi /etc/network/interfaces
iface eth0 inet static
address IP
netmask 掩码
gateway 网关
dns-nameservers DNS或网关

vi /etc/resolve.conf
nameserver DNS
```

* 工具安装
```Bash
mkdir /package && cd /package
apt-get install -y wget
wget https://nodejs.org/dist/v4.5.0/node-v4.5.0-linux-armv7l.tar.gz
tar -xvzf ./node-v4.5.0-linux-armv7l.tar.gz
mv ./node-v4.5.0-linux-armv7l /usr/local/nodejs
```

* 环境变量
```Bash
vi /etc/environment
PATH="/usr/local/nodejs/bin:原始内容"
```

* 淘宝源及必要库
```Bash
npm config set registry https://registry.npm.taobao.org
npm install -g node-gyp
```