"use strict";

let THIS;

let NFN = (()=> {
	let

	function NFN () {
		THIS = this;

		THIS.init();

		return THIS;
	}

	NFN.prototype = {
		init:   ()=> {

		},
		uninit: ()=> {

		}
	};

	return NFN;
})
();
// 通讯设置 水务控制, 门禁控制, 传感器控制
// (require("nfn-cp210x"), require("eit"));

module.exports = exports = (cfg, error) => {
	return THIS ? THIS : new NFN(cfg, error);
};

return (cfg, error, destory)=> {
	return THIS ? THIS : new NFN(cfg, error, destory);
};