"use strict";
let serial = require("nfn-serial"),
	arg    = process.argv.splice(2)[0] || "tty.usbserial-00001014A";

serial.init(
	{
		name: "/dev/" + arg,
		baud: 9600,
		hex:  false
	},
	{
		"open":  (name, cfg)=> console.log(`[open] name: ${name} cfg: ${cfg}`),
		"close": ()=> console.log("[close]"),
		"read":  (buf, len)=> {
			if (typeof(buf) == "string") {
				console.log(`[read] buf:${buf} len: ${len}`);

				switch (buf) {
					case "reset":
						com.reset();
						break;
					case "close":
						com.close();
						break;
					case "hello":
						break;
				}
			}
			else {
				console.log("[read] buf:", buf, " len:", len);
			}
		},
		"write": len=> console.log("[write] len: ", len)
	}
).open();